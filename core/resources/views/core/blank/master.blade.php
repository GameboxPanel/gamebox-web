<?php
?>
        <!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Gamebox Panel</title>

    <link href="/assets/img/favicon.144x144.html" rel="apple-touch-icon" type="image/png" sizes="144x144">
    <link href="/assets/img/favicon.114x114.html" rel="apple-touch-icon" type="image/png" sizes="114x114">
    <link href="/assets/img/favicon.72x72.html" rel="apple-touch-icon" type="image/png" sizes="72x72">
    <link href="/assets/img/favicon.57x57.html" rel="apple-touch-icon" type="image/png">
    <link href="/assets/img/favicon.html" rel="icon" type="image/png">
    <link href="/assets/img/favicon-2.html" rel="shortcut icon">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/main.css">

    <style>
        .col-centered {
            float:none;
            margin:0 auto;
        }
    </style>

    @yield('head')
</head>
<body>

@yield('content')


@yield('bottom')
<script src="/assets/js/app.js"></script>
</body>

<!-- Mirrored from themesanytime.com/startui/demo/sign-in.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 17 May 2016 00:36:06 GMT -->
</html>
