<?php
?>
@extends('core.blank.master')

@section('head')
    <style>
        .smallinput {
            width:10%;
        }
    </style>
    <link rel="stylesheet" href="/assets/css/lib/font-awesome/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/css/lib/bootstrap-sweetalert/sweetalert.css"/>
@endsection

@section('content')
    <div class="page-center">
        <div class="row">
            <div class="col-sm-7 col-centered" style="padding-top:10em;">
                <section class="card card-default">
                    <header class="card-header">Validation License</header>
                    <div class="card-block">

                        <p>
                            You need to validate your license to continue with the installation. If you do not have a valid license key please get one and enter it here.
                        </p>

                        <div class="alert alert-warning alert-icon">
                            <i class="font-icon font-icon-warning">
                            </i>
                            The alpha versions are FREE. Enter <code>FREE-FREE-FREE-FREE</code> for the license code.
                        </div>

                        <form action="/install/step/4" method="post" id="form">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <div class="row">
                                <div class="col-lg-6 col-centered text-center">
                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-key"></i>
                                            </div>
                                            <input type="text" name="license" placeholder="XXXX-XXXX-XXXX-XXXX" class="form-control" id="license">
                                            <div class="input-group-btn">
                                                <button type="button" class="btn btn-default" onclick="checkLicense();">Check</button>
                                            </div>
                                        </div>
                                        <small class="text-muted"></small>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 text-left">
                                    <button class="btn btn-default btn-inline" disabled>Back</button>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <button class="btn btn-primary btn-inline" id="finishbtn" disabled>Finish Installation</button>
                                </div>
                            </div>
                        </form>

                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('bottom')
    <script src="/assets/js/lib/jquery/jquery.min.js"></script>
    <script src="/assets/js/lib/tether/tether.min.js"></script>
    <script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="/assets/js/plugins.js"></script>

    <script src="/assets/js/lib/input-mask/jquery.mask.min.js"></script>

    <script src="https://malsup.github.com/jquery.form.js"></script>

    <script src="/assets/js/lib/bootstrap-sweetalert/sweetalert.min.js"></script>

    <script type="text/javascript">
        var valid = false;

        $(function() {
            $("#license").mask("AAAA-AAAA-AAAA-AAAA",{placeholder: "XXXX-XXXX-XXXX-XXXX"});

            $("#form").ajaxForm({
                dataType: 'json',
                success: process,
                beforeSubmit: function() {

                }
            });
        });

        function checkLicense() {
            var data = $("#license").val();

            $.get("/ajax/install/checklicense/"+encodeURIComponent(data),function(data) {
                if(data == "valid") {
                    setState("success","license","License key is valid.");
                    valid = true;
                    $("#finishbtn").prop("disabled",false);
                }
                else if(data == "invalid") {
                    setState("error","license","License key is invalid.");
                    valid = false;
                    $("#finishbtn").prop("disabled",true);
                }
            });
        }

        function setState(state,field,msg) {
            if(state == "success") {
                setState("none",field,msg);
                $("#"+field).parent().parent().addClass("form-group-success");
                $("#"+field).parent().next().html(msg);
            }
            else if(state == "none") {
                $("#"+field).parent().parent().removeClass("form-group-success");
                $("#"+field).parent().parent().removeClass("form-group-error");
                $("#"+field).parent().next().html("");
            }
            else if(state == "error") {
                $("#"+field).parent().parent().addClass("form-group-error");
                $("#"+field).parent().next().html(msg);
            }
        }

        function process(data) {
            if(data.errors == true) {
                if(data.license.error == true) {
                    setState("error","license",data.license.msg);
                } else {
                    setState("none","license","");
                }
            } else {
                swal({
                    title: 'Congrats, You have finished the installation!',
                    test: 'Everything looks good, you can now sign in to the control panel to start setting up game servers.',
                    type: 'success',
                    confirmClassButton: 'btn-primary',
                    confirmButtonText: 'Finish Installation',
                    showCancelButton: false,
                    closeOnConfirm: false
                }, function() {
                    window.location = '/install/finish';
                });
            }
        }
    </script>
@endsection