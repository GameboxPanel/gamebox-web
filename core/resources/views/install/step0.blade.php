<?php
function getCheckmark($set) {

    if($set == false) {
        echo "<i class='text-danger font-icon font-icon-del'></i> ";
    } else {
        echo "<i class='text-success font-icon font-icon-ok'></i> ";
    }
}
?>
@extends('core.blank.master')

@section('head')
    <style>
    </style>
    <link rel="stylesheet" href="/assets/css/lib/bootstrap-sweetalert/sweetalert.css"/>
@endsection

@section('content')
    <div class="page-center">
        <div class="row">
            <div class="col-sm-7 col-centered" style="padding-top:10em;">

                <section class="maincenter card card-default">
                    <header class="card-header">Installation Checks</header>
                    <div class="card-block">
                        <p>
                            Thank you for installing Gamebox. We are glad you are using us to take control of your gaming experience. We won't let you down!<br>
                            Below are a few things you will need to check before we get started!
                        </p>

                        <div class="">
                            <strong>Please check everything.</strong>
                            <ul>
                                <li>{{getCheckmark($checks["database"])}} - Database Connection</li>
                                <li>{{getCheckmark($checks["fsock"])}} - FSockOpen Enabled</li>
                                <li>{{getCheckmark($checks["mail"])}} - PHP Mail Enabled</li>
                            </ul>
                        </div>
                        <br>
                        <p>
                            If you are having trouble, be sure to read the installation tutorial. <a href="https://gameboxpanel.ga/installation">https://gameboxpanel.ga/installation</a>
                        </p>
                        <blockquote class="blockquote">
                            <p>
                                If you are having trouble getting the database connection to succeed make sure you set your mysql database information in the <code>.env</code> file that is located at <code>/core/.env</code>. This is where the primary database to use with Gamebox is stored. Keep this file secret or encrypt it with ioncube.
                            </p>
                        </blockquote><br>
                        <div class="row">
                            <div class="col-xs-6 text-left">
                                <button class="btn btn-inline" disabled>Back</button>
                            </div>
                            <div class="col-xs-6 text-right">
                                @if($continue == true)
                                    <button class="btn btn-inline" onclick="window.location='/install/step/1';">Continue</button>
                                @else
                                    <button class="btn btn-inline" onclick="window.location='/install';">Check Again</button>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
    </div>
@endsection

@section('bottom')
    <script src="/assets/js/lib/jquery/jquery.min.js"></script>
    <script src="/assets/js/lib/tether/tether.min.js"></script>
    <script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="/assets/js/plugins.js"></script>

    <script src="/assets/js/lib/bootstrap-sweetalert/sweetalert.min.js"></script>

    @if($popup == false)
    <script type="text/javascript">
        $(window).bind("load", function() {
            swal({
               title: "Thank you for using Gamebox!",
                text: "The next step is to install so you can get your games running right away.",
                type: "info",
                showCancelButton: false,
                confirmButtonClass: "btn-default",
                confirmButtonText: "I'm Ready!"
            });
        });
    </script>
    @endif
@endsection
