<?php
?>
@extends('core.blank.master')

@section('head')
    <link rel="stylesheet" href="/assets/css/lib/bootstrap-sweetalert/sweetalert.css"/>
@endsection

@section('content')
    <div class="page-center">
        <div class="row">
            <div class="col-sm-7 col-centered" style="padding-top:10em;">
                <section class="card card-default">
                    <header class="card-header">Database Installation</header>
                    <div class="card-block">
                        <p>
                            Our next step is to install the default database. This will setup the framework and stop errors. This step is critical and issues with this step will cause errors. Be sure that FSockOpen is working as this will contact our main servers for any database structure updates that may of been pushed since the last download.<br>
                            Also note that most of the sensitive information such as passwords will be encrypted in the database with 256bit AES encryption which is known as one of the most unbreakable encryption schemes.
                        </p>
                        <div class="text-center">
                            <button class="btn btn-inline btn-primary" onclick="createDatabase();">Install Database</button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('bottom')
    <script src="/assets/js/lib/jquery/jquery.min.js"></script>
    <script src="/assets/js/lib/tether/tether.min.js"></script>
    <script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="/assets/js/plugins.js"></script>

    <script src="/assets/js/lib/bootstrap-sweetalert/sweetalert.min.js"></script>
    <script type="text/javascript" src="/assets/js/lib/blockUI/jquery.blockUI.js"></script>

    <script type="text/javascript">
        var timeout;

        function createDatabase() {
            $.blockUI({
                overlayCSS: {
                    background: 'rgba(24, 44, 68, 0.8)',
                    opacity: 1,
                    cursor: 'wait'
                },
                css: {
                    width: 'auto',
                    top: '45%',
                    left: '45%'
                },
                message: '<div class="blockui-default-message"><i class="fa fa-circle-o-notch fa-spin"></i><h6>Installing Database</h6></div>',
                blockMsgClass: 'block-msg-default'
            });

            $.get('/ajax/install/installDatabase', function(data) {
                data = eval("("+data+")");

                if(data.errors == false) {
                    clearTimeout(timeout);
                    setTimeout($.unblockUI, 100);
                    setTimeout("displaySuccess()",500);
                }
            });

            timeout = setTimeout("timeoutRequest()", 5000);
        }

        function displaySuccess() {
            swal({
                title: "Database Successfully Installed!",
                text: "Everything looks good, we are almost done.",
                type: "success",
                confirmButtonClass: "btn-success",
                confirmButtonText: "Next Step",
                closeOnConfirm: false,
                showCancelButton: false
            }, function() {
                window.location = '/install/step/2';
            });
        }

        function timeoutRequest() {
            setTimeout($.unblockUI, 10);
            setTimeout(function() {
                swal({
                    title: "Oh No! Errors!",
                    text: "There were errors with the connection.",
                    type: "error",
                    confirmButtonClass: "btn-default",
                    confirmButtonText: "Try Again",
                    showCancelButton: false
                });
            }, 750);
        }
    </script>
@endsection
