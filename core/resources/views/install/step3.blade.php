<?php
?>
@extends('core.blank.master')

@section('head')
    <link rel="stylesheet" href="/assets/css/lib/ladda-button/ladda-themeless.min.css">
@endsection

@section('content')
    <div class="page-center">
        <div class="row">
            <div class="col-sm-7 col-centered" style="padding-top:10em;">
                <section class="card card-default">
                    <header class="card-header">Admin Account Setup</header>
                    <div class="card-block">
                        <p>
                            The next step is to create the admin account that you will manage the game panel from. Be sure that you remember this account's details as it may be hard to recover it.
                        </p>

                        <form action="/install/step/3" method="post" id="form">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">

                            <div class="row">
                                <div class="col-xs-10 col-centered">

                                    <div class="form-group row">
                                        <label class="form-control-label col-sm-3">Username</label>
                                        <div class="col-sm-9">
                                            <input type="text" name="username" class="form-control" id="username" placeholder="Desired Username">
                                            <small class="text-muted"></small>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="form-control-label col-sm-3">E-Mail Address</label>
                                        <div class="col-sm-9">
                                            <input type="email" name="email" class="form-control" id="email" placeholder="E-Mail Address">
                                            <small class="text-muted"></small>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="form-control-label col-sm-3">
                                            Password
                                        </label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Password" id="password" name="password">
                                            <small class="text-muted"></small>
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label class="form-control-label col-sm-3">Retype Password</label>
                                        <div class="col-sm-9">
                                            <input type="password" class="form-control" placeholder="Retype Password" id="confirm" name="confirm">
                                            <small class="text-muted"></small>
                                        </div>
                                    </div>

                                </div>
                            </div>

                            <div class="row">
                                <div class="col-xs-6 text-left">
                                    <button class="btn btn-inline" disabled>Back</button>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <button type="submit" id="submitbutton" class="btn btn-primary btn-inline ladda-button" data-style="expand-left">Create Account</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('bottom')
    <script src="/assets/js/lib/jquery/jquery.min.js"></script>
    <script src="/assets/js/lib/tether/tether.min.js"></script>
    <script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="/assets/js/plugins.js"></script>

    <script src="https://malsup.github.com/jquery.form.js"></script>

    <script src="/assets/js/lib/ladda-button/spin.min.js"></script>
    <script src="/assets/js/lib/ladda-button/ladda.min.js"></script>

    <script src="/assets/js/lib/pnotify/pnotify.js"></script>

    <script type="text/javascript">
        $(function() {
           $("#form").ajaxForm({
               dataType: 'json',
               success: process,
               beforeSubmit: function() {
                   Ladda.create(document.querySelector("#submitbutton")).start();
               }
           });

            Ladda.bind('button.ladda-button',{timeout: 5000});
            PNotify.prototype.options.styling = 'bootstrap3';
        });

        function process(data) {
            if(data.errors == true) {
                if(data.username.error == true) {
                    setState("error","username",data.username.msg);
                } else {
                    setState("none","username","");
                }

                if(data.email.error == true) {
                    setState("error","email",data.email.msg);
                } else {
                    setState("none","email","");
                }

                if(data.password.error == true) {
                    setState("error","password",data.password.msg);
                } else {
                    setState("none","password","");
                }

                if(data.confirm.error == true) {
                    setState("error","confirm",data.confirm.msg);
                } else {
                    setState("none","confirm","");
                }

                Ladda.create(document.querySelector("#submitbutton")).stop();
            }
            else {
                new PNotify({
                    title: 'Account Created!',
                    text: 'The admin account for you game panel has been created.',
                    type: 'success',
                    icon: 'font-icon font-icon-ok',
                    addClass: 'alert-with-icon alert-grey'
                });

                setTimeout(function() { window.location = '/install/step/4'; },2500);
            }
        }

        function setState(state,field,msg) {
            if(state == "error") {
                setState("none",field,msg);
                $("#"+field).parent().parent().addClass("form-group-error");
                $("#"+field).next().html(msg);
            }
            else if(state == "none") {
                $("#"+field).parent().parent().removeClass("form-group-error");
                $("#"+field).next().html("");
            }
        }
    </script>
@endsection