<?php
?>
@extends('core.blank.master')

@section('content')
    <div class="page-center">
        <div class="row">
            <div class="col-sm-7 col-centered" style="padding-top:10em;">
                <section class="card card-default">
                    <header class="card-header">Installation is complete!</header>
                    <div class="card-block">
                        <p>
                            Contragts, you have successfully installed Gamebox and it is ready for use. You may now click the button below to log into the control panel with the admin information you entered during setup.<br>
                            Please note that there is no support for the free or trial versions of this software. The alpha version may have community support so if you have discover any bugs or have general questions, feel free to do some research before opening an issue on our github page.<br>
                            <br>
                        </p>

                        <div class="text-center">
                            <button class="btn btn-primary btn-block btn-lg" onclick="window.location = '/';">Finish Installation</button>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection
