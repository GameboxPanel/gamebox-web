<?php
?>
@extends('core.blank.master')

@section('head')
    <script type="text/javascript">
        function updateCheck(element, state) {
            if($(element).is(":checked")) {
                $("#"+state).html("Enabled");
            } else {
                $("#"+state).html("Disabled");
            }
        }
    </script>
    <link rel="stylesheet" href="/assets/css/lib/ladda-button/ladda-themeless.min.css">
@endsection

@section('content')
    <div class="page-center">
        <div class="row">
            <div class="col-sm-7 col-centered" style="padding-top:10em;">
                <section class="card card-default">
                    <header class="card-header">Installation Configuration</header>
                    <div class="card-block">
                        <p>
                            The next step is to configure the game panel to suit your needs. All of the information that you enter below will be used to personalize your installation. Also remember as we push more updates, more configuration options will be available. You can always edit these later.
                        </p>

                        <form id="form" class="form" action="/install/step/2" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Game Panel Name</label>
                                <div class="col-sm-9">
                                    <input type="text" id="name" name="name" placeholder="Game Panel Name" class="form-control">
                                    <small class="text-muted"></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Website URL</label>
                                <div class="col-sm-9">
                                    <input type="text" id="websiteurl" name="websiteurl" class="form-control" placeholder="Main Website URL">
                                    <small class="text-muted"></small>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Gamebox Panel URL</label>
                                <div class="col-sm-9">
                                    <input type="text" id="gamepanelurl" name="gamepanelurl" class="form-control" placeholder="Game Panel URL">
                                    <small class="text-muted"></small>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">Force HTTPS</label>
                                <div class="col-sm-9">
                                    <div class="checkbox-toggle">
                                        <input type="checkbox" name="sslenable" value="yes" id="checkbox-toggle-1" onclick="updateCheck(this,'sslenable');">
                                        <label id="sslenable" for="checkbox-toggle-1">Disabled</label>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-3 form-control-label">CloudFlare Enabled</label>
                                <div class="col-sm-9">
                                    <div class="checkbox-toggle">
                                        <input type="checkbox" name="cloudflareenable" value="yes" id="checkbox-toggle-2" onclick="updateCheck(this,'cloudflareenable');">
                                        <label id="cloudflareenable" for="checkbox-toggle-2">Disabled</label>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-6">
                                    <button class="btn btn-inline" disabled>Back</button>
                                </div>
                                <div class="col-sm-6 text-right">
                                    <button class="savebutton btn btn-inline btn-primary ladda-button" data-style="expand-left" type="submit">Save Changes</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('bottom')
    <script src="/assets/js/lib/jquery/jquery.min.js"></script>
    <script src="/assets/js/lib/tether/tether.min.js"></script>
    <script src="/assets/js/lib/bootstrap/bootstrap.min.js"></script>
    <script src="/assets/js/plugins.js"></script>

    <script src="https://malsup.github.com/jquery.form.js"></script>

    <script src="/assets/js/lib/pnotify/pnotify.js"></script>

    <script src="/assets/js/lib/ladda-button/spin.min.js"></script>
    <script src="/assets/js/lib/ladda-button/ladda.min.js"></script>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#form").ajaxForm({
                dataType: 'json',
                success: processSave,
                beforeSubmit: function() {
                    Ladda.create(document.querySelector(".savebutton")).start();
                }
            });

            PNotify.prototype.options.styling = "bootstrap3";
            Ladda.bind('button.ladda-button',{timeout: 5000});
        });

        function processSave(data) {
            var errors = false;
            if(data.name.error == true) {
                setState("error","name",data.name.ret);
                errors = true;
            } else {
                setState("none","name","");
                setState("success","name","");
            }

            if(data.websiteurl.error == true) {
                setState("error","websiteurl",data.websiteurl.ret);
                errors = true;
            }

            if(data.gamepanelurl.error == true) {
                setState("error","gamepanelurl",data.gamepanelurl.ret);
                errors = true;
            }

            if(errors == true) {
                new PNotify({
                    title: 'Oh No!',
                    text: 'There were errors with the setting you are trying to save. Please try again.',
                    type: 'error',
                    icon: 'font-icon font-icon-warning',
                    addClass: 'alert-with-icon alert-grey'
                });
                Ladda.create(document.querySelector(".savebutton")).stop();
            } else {
                new PNotify({
                    title: 'Settings Saved Successfully!',
                    text: 'The setting you have entered have been saved.',
                    type: 'success',
                    icon: 'font-icon font-icon-ok',
                    addClass: 'alert-with-icon alert-grey'
                });
                setTimeout(function() {
                    window.location = '/install/step/3';
                },2500);
            }
        }

        function setState(state, field, message) {
            if(state == "error") {
                $("#"+field).parent().parent().addClass("form-group-error");
                $("#"+field).next().html(message);
            }
            else if(state == "success") {
                $("#"+field).parent().parent().addClass("form-group-success");
                $("#"+field).next().html("");
            }
            else if(state == "none") {
                $("#"+field).parent().parent().removeClass("form-group-error");
                $("#"+field).parent().parent().removeClass("form-group-success");
                $("#"+field).next().html("");
            }
        }
    </script>
@endsection