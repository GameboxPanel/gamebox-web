<?php
namespace App\Core\Framework\Install;
use DB;

class InstallCheck {
    public function checkInstallState() {
        $install = false;
        
        try {
            if(DB::connection()) {
                $install = true;
            } else {
                $install = false;
            }
        } catch(\Exception $e){
            $install = false;
        }
        
        try {
            $aon = DB::table("site")->get();

            if($aon[0]->installed == true) {
                $install = true;
            } else {
                $install = false;
            }
        } catch(\Exception $e) {
            $install = false;
        }
        
        return $install;
    }
}