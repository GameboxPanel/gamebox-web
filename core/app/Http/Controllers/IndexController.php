<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Core\Framework\Install\InstallCheck;

class IndexController extends Controller {
    public function index() {
        $check = new InstallCheck;
        
        $installed = $check->checkInstallState();
        
        if($installed == false) {
            return redirect()->route('install:index');
        }
    }
}