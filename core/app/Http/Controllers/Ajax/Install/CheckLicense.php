<?php
namespace App\Http\Controllers\Ajax\Install;

use App\Http\Controllers\Controller;
use App\Core\Validation\Libs\GUMP;

class CheckLicense extends Controller {
    public function index($license = "") {
        $data = array(
            "license" => $license
        );
        
        $data = GUMP::filter_input($data, array(
            "license" => "trim|sanitize_string"
        ));
        
        $freeLicense = "FREE-FREE-FREE-FREE";
        
        $gump = new GUMP();
        $check = $gump->is_valid($data, array(
            "license" => "required|min_len,19|max_len,19"
        ));
        
        $error = false;
        $msg = "";
        
        GUMP::set_field_name("license","License Key");
        
        if($check !== true) {
            $error = true;
            $msg = $check[0];
        }
        
        //Check key against server
        
        elseif($data["license"] == $freeLicense) {
            $error = false;
        } else {
            $error = true;
        }
        
        
        
        if($error == false) {
            echo "valid";
        } else {
            echo "invalid";
        }
    }
}