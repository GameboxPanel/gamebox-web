<?php
namespace App\Http\Controllers\Ajax\Install;

use App\Http\Controllers\Controller;
use DB;
use Schema;
use Storage;

class InstallDatabase extends Controller {
    public $error = false;
    public function index() {
        try {
            if(DB::connection() && Schema::hasTable('site') == false) {
               $this->installDatabase();
            } else {
                $this->error = true;
            }
        } catch(\Exception $e) {
            $this->error = true;
        }
        
        echo json_encode(array("errors" => $this->error));
    }

    private function installDatabase() {
        if($this->error == false) {
            try {
                $sql = Storage::get('/sql/DB.sql');
                DB::unprepared($sql);
            } catch(Exception $e) {
                $this->error = true;
            }
        }
    }
}