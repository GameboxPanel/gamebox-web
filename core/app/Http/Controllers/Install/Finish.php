<?php
namespace App\Http\Controllers\Install;

use App\Http\Controllers\Controller;
use DB;

class Finish extends Controller {
    public function index() {
        $installed = false;

        // Check if the installation is finished.
        $aon = DB::table("site")->where("id","1")->select("installed")->get();

        if(count($aon) == 0) {
            $installed = false;
        }
        else if($aon[0]->installed == true) {
            $installed = true;
        }
        
        return view('install.finish',array("installed" => $installed));
    }
}