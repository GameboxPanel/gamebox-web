<?php
namespace App\Http\Controllers\Install;

use App\Http\Controllers\Controller;
use App\Core\Validation\Libs\GUMP;
use DB;

class Step2 extends Controller {
    public function index() {
        return view('install.step2');
    }
    
    public function post()
    {
        $data = GUMP::filter_input($_POST, array(
            "name" => "trim|sanitize_string",
            "websiteurl" => "trim|sanitize_string",
            "gamepanelurl" => "trim|sanitize_string",
        ));

        $ret = array(
            "name" => array(
                "error" => false,
                "ret" => null
            ),
            "websiteurl" => array(
                "error" => false,
                "ret" => null
            ),
            "gamepanelurl" => array(
                "error" => false,
                "ret" => null
            )
        );
        
        $errors = false;

        $ssl = false;
        $cloudflare = false;

        if (isset($_POST['sslenable'])) {
            $ssl = true;
        }

        if (isset($_POST['cloudflareenable'])) {
            $cloudflare = true;
        }


        //Check Name
        $check = GUMP::is_valid($data, array(
            "name" => "required"
        ));

        if($check !== true) {
            $ret["name"]["error"] = true;
            $ret["name"]["ret"] = "Name must not be blank.";
            $errors = true;
        }

        $check = GUMP::is_valid($data, array(
            "websiteurl" => "required|valid_url"
        ));
        
        if($check !== true) {
            $ret["websiteurl"]["error"] = true;
            $ret["websiteurl"]["ret"] = "Website URL must be valid.";
            $errors = true;
        }
        
        $check = GUMP::is_valid($data, array(
            "gamepanelurl" => "required|valid_url"
        ));
        
        if($check !== true) {
            $ret["gamepanelurl"]["error"] = true;
            $ret["gamepanelurl"]["ret"] = "Game panel URL must be valid.";
            $errors = true;
        }
        
        if($errors == true) {
            echo json_encode($ret);
        } else {
         // Update the database.
            echo json_encode($ret);

            DB::table('site')->where("id","1")->update(array(
                "name" => $data["name"],
                "sslenable" => $ssl,
                "cloudflare" => $cloudflare,
                "websiteurl" => $data["websiteurl"],
                "gamepanelurl" => $data["gamepanelurl"]
            ));
        }
    }
}