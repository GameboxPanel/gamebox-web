<?php
namespace App\Http\Controllers\Install;

use App\Http\Controllers\Controller;
use App\Core\Validation\Libs\GUMP;
use DB;
use Hash;
use App\Core\Framework\UUID\UUID;

class Step3 extends Controller {
    public function index() {
        return view('install.step3');
    }
    
    public function post() {
        $data = GUMP::filter_input($_POST, array(
            "username" => "trim|sanitize_string",
            "email" => "trim|sanitize_string",
            "password" => "trim|sanitize_string",
            "confirm" => "trim|sanitize_string"
        ));

        $ret = array(
            "errors" => false,
            "username" => array(
                "error" => false,
                "msg" => null
            ),
            "email" => array(
                "error" => false,
                "msg" => null
            ),
            "password" => array(
                "error" => false,
                "msg" => null
            ),
            "confirm" => array(
                "error" => false,
                "msg" => null
            )
        );
        
        $gump = new GUMP();
        $check = $gump->is_valid($data, array(
            "username" => "required|alpha_numeric|min_len,4"
        ));

        if($check !== true) {
            $ret["errors"] = true;
            $ret["username"]["error"] = true;
            $ret["username"]["msg"] = $check[0];
        } else {
            $aon = DB::table("users")->where("username",$data["username"])->get();
            
            if(count($aon) > 0) {
                $ret["errors"] = true;
                $ret["username"]["error"] = true;
                $ret["username"]["msg"] = "That username is already in use.";
            }
        }
        
        $gump = new GUMP();
        $check = $gump->is_valid($data, array(
            "email" => "required|valid_email"
        ));
        
        if($check !== true) {
            $ret["errors"] = true;
            $ret["email"]["error"] = true;
            $ret["email"]["msg"] = $check[0];
        } else {
            $aon = DB::table("users")->where("email",$data["email"])->get();
            if(count($aon) > 0) {
                $ret["errors"] = true;
                $ret["email"]["error"] = true;
                $ret["email"]["msg"] = "That E-Mail address already exists.";
            }
        }
        
        $gump = new GUMP();
        $check = $gump->is_valid($data, array(
            "password" => "required|min_len,6"
        ));
        if($check !== true) {
            $ret["errors"] = true;
            $ret["password"]["error"] = true;
            $ret["password"]["msg"] = $check[0];
        }
        else if($data["password"] != $data["confirm"]) {
            $ret["errors"] = true;
            $ret["password"]["error"] = true;
            $ret["password"]["msg"] = "Your passwords do not match.";
        }
        
        $gump = new GUMP();
        $check = $gump->is_valid($data, array(
            "confirm" => "required|min_len,6"
        ));
        if($check !== true) {
            $ret["errors"] = true;
            $ret["confirm"]["error"] = true;
            $ret["confirm"]["msg"] = $check[0];
        }
        else if($data["confirm"] != $data["password"]) {
            $ret["errors"] = true;
            $ret["confirm"]["error"] = true;
            $ret["confirm"]["msg"] = "Your passwords do not match.";
        }
        
        if($ret["errors"] == true) {
            echo json_encode($ret);
        } else {
            echo json_encode($ret);
            
            $uuid = UUID::v4();
            
            DB::table("users")->insert(array(
                "username" => $data["username"],
                "email" => $data["email"],
                "admin" => true,
                "password" => Hash::make($data["password"]),
                "userid" => $uuid
            ));
        }
    }
}