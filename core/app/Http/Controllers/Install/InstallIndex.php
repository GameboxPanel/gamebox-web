<?php
namespace App\Http\Controllers\Install;

use App\Http\Controllers\Controller;
use DB;
use Request;
use Cookie;

class InstallIndex extends Controller {
    public $checks = array(
        "database" => false,
        "fsock" => false,
        "phpversion" => false,
        "mail" => false
    );
    
    public $continue = true;

    public $popup = false;
    
    private function init() {
        // Perform checks
        
        try {
            //Database Check
            if(DB::connection()) {
                $this->checks["database"] = true;
            } else {
                $this->checks["database"] = false;
                $this->continue = false;
            }
            
        } catch(\Exception $e) {
            $this->checks["database"] = false;
            $this->continue = false;
        }

        // Fsockopen
        if(function_exists("fsockopen")) {
            $this->checks["fsock"] = true;
        } else {
            $this->checks["fsock"] = false;
            $this->continue = false;
        }

        // Mail
        if(function_exists("mail")) {
            $this->checks["mail"] = true;
        } else {
            $this->checks["mail"] = false;
            $this->continue = false;
        }

        //Set popup cookie
        if(isset($_COOKIE['installCookie'])) {
            $this->popup = true;
        } else {
            Cookie::queue("installCookie","yes",time()+60*60*3);
            $this->popup = false;
        }
    }
    
    public function index() {
        $this->init();
        
        return view('install.step0', array("checks" => $this->checks, "continue" => $this->continue, "popup" => $this->popup));
    }
}