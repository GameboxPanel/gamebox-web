<?php
namespace App\Http\Controllers\Install;

use App\Http\Controllers\Controller;
use App\Core\Validation\Libs\GUMP;
use App\Core\Framework\License\LicenseValidate;
use DB;

class Step4 extends Controller {
    public function index() {
        return view('install.step4');
    }

    public function post() {
        $data = GUMP::filter_input($_POST, array(
            "license" => "trim|sanitize_string"
        ));
        
        $ret = array(
            "errors" => false,
            "license" => array(
                "error" => true,
                "msg" => ""
            )
        );
        
        $gump = new GUMP();
        $check = $gump->is_valid($data, array(
            "license" => "required"
        ));
        if($check !== true) {
            $ret["errors"] = true;
            $ret["license"]["error"] = true;
            $ret["license"]["msg"] = $check[0];
        }
        
        if($ret["errors"] == true) {
            echo json_encode($ret);
        } else {
            echo json_encode($ret);
            
            DB::table("site")->where("id","1")->update(array(
                "installed" => true
            ));
        }
    }
}