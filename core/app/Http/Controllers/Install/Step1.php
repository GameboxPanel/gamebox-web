<?php
namespace App\Http\Controllers\Install;

use App\Http\Controllers\Controller;

class Step1 extends Controller {
    public function index() {
        return view('install.step1');
    }
}