<?php
Route::get('/',['as' => 'index', 'uses' => 'IndexController@index']);

// Install
Route::group(['middleware' => array()], function() {
    Route::get('install',['as' => 'install:index','uses' => 'Install\InstallIndex@index']);
    Route::get('install/step/1',['as' => 'install:step1','uses' => 'Install\Step1@index']);
    Route::get('install/step/2',['as' => 'install:step2','uses' => 'Install\Step2@index']);
    Route::post('install/step/2',['as' => 'install:post:step2', 'uses' => 'Install\Step2@post']);
    Route::get('install/step/3',['as' => 'install:step3', 'uses' => 'Install\Step3@index']);
    Route::post('install/step/3',['as' => 'install:step3:post', 'uses' => 'Install\Step3@post']);
    Route::get('install/step/4',['as' => 'install:step4', 'uses' => 'Install\Step4@index']);
    Route::post('install/step/4',['uses' => 'Install\Step4@post']);
    Route::get('install/finish',['uses' => 'Install\Finish@index']);
    
    //Ajax
    Route::get('ajax/install/installDatabase', ['as' => 'ajax:install_installDatabase', 'uses' => 'Ajax\Install\InstallDatabase@index']);
    Route::get('ajax/install/checklicense/{license?}', ['uses' => 'Ajax\Install\CheckLicense@index']);
});
